CREATE TABLE planta (
	id_p int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	nombre varchar NULL,
	weather_apikey varchar NULL,
	sunapi_apikey varchar NULL,
	solar_ruta varchar NULL
);

insert into planta (nombre, weather_apikey, sunapi_apikey,solar_ruta ) values ('Lider Express', 'Chanchito','Feliz','');
insert into planta (nombre, weather_apikey, sunapi_apikey,solar_ruta ) values ('Tottus la florida', 'Chanchito','Triste','');
insert into planta (nombre, weather_apikey, sunapi_apikey,solar_ruta ) values ('Unimar puente alto', 'Gato','Dormilon','');
insert into planta (nombre, weather_apikey, sunapi_apikey,solar_ruta ) values ('Estacionamiento', 'Perrito','Sunai','');
insert into planta (nombre, weather_apikey, sunapi_apikey,solar_ruta ) values ('Jumbo san felipe', 'Tigre','Trillaba','');

CREATE TABLE inversor (
	id_i int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	id_p int4 NOT NULL,
	marca varchar NULL,
	modelo varchar NULL,
	serial_number varchar NULL
);

CREATE TABLE inversor_medicion (
	id_im int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	id_i int4 NOT NULL,
	fecha timestamp NULL,
	apparent_power numeric NULL,
	ac_current numeric NULL,
	dc_power numeric NULL,
	dc_voltage numeric NULL,
	active_energy numeric NULL,
	active_power numeric NULL,
	reactive_power numeric NULL,
	cos_phi numeric NULL
);

CREATE OR REPLACE FUNCTION public.ins_inversor_medicion(p_marca_inversor text, p_modelo_inversor text, p_serial_number_inversor text, p_fecha timestamp without time zone, p_apparent_power numeric, p_ac_current numeric, p_dc_power numeric, p_dc_voltage numeric, p_active_energy numeric, p_active_power numeric, p_reactive_power numeric,p_cos_phi numeric, OUT id_im integer) RETURNS integer
 LANGUAGE plpgsql
AS $function$
declare
	v_id_i int4;
	id_im int4;
begin
	if not exists(select 1 from inversor where marca = p_marca_inversor and modelo = p_modelo_inversor and serial_number = p_serial_number_inversor) then
		-- Creo el inversor si no existe
		insert into inversor (id_p, marca, modelo, serial_number)
		values (p_marca_inversor, p_modelo_inversor, p_serial_number_inversor);
		raise info 'Se inserta inversor, ya que no existia, %', now();
		-- Inserto los datos de medicion con el inversor recien insertado
		select currval('inversor_id_i_seq') into v_id_i;
		insert into inversor_medicion(id_i, fecha, apparent_power, ac_current, dc_power, dc_voltage, active_energy, active_power, reactive_power, cos_phi)
		values (v_id_i, p_fecha, p_apparent_power, p_ac_current, p_dc_power, p_dc_voltage, p_active_energy, p_active_power, p_reactive_power, p_cos_phi);
		id_im := currval('inversor_medicion_id_im_seq');
	else
		-- Obtiene datos de inversor
		select id_i
		into v_id_i
		from inversor where marca_i = p_marca_inversor and modelo_i = p_modelo_inversor and serial_number_i = p_serial_number_inversor;
		
		if exists (select 1 from inversor_medicion where id_i = v_id_i and fecha_im = p_fecha) then
			raise warning 'Ya existen datos para el inversor y fecha indicado, %', now();
			id_im := -1;
		else
			insert into inversor_medicion(id_i, fecha, apparent_power, ac_current, dc_power, dc_voltage, active_energy, active_power, reactive_power, cos_phi)
			values (v_id_i, p_fecha, p_apparent_power, p_ac_current, p_dc_power, p_dc_voltage, p_active_energy, p_active_power, p_reactive_power, p_cos_phi);
			id_im := currval('inversor_medicion_id_im_seq');
		end if;
	end if;
end;
$function$
;